import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import axios from 'axios'

Vue.use(Buefy);

Vue.config.productionTip = false;
axios.defaults.baseURL = "http://ec2-35-178-104-246.eu-west-2.compute.amazonaws.com";

new Vue({
    router,
    render: function (h) { return h(App) }
}).$mount('#app')