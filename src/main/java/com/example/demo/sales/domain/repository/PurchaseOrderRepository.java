package com.example.demo.sales.domain.repository;

import com.example.demo.sales.domain.model.POStatus;
import com.example.demo.sales.domain.model.PurchaseOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Collection;
import java.util.stream.Stream;

@Repository
public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, Long> {
    Collection<PurchaseOrder> findByCustomerName(String name);
    Collection<PurchaseOrder> findByStatus(POStatus status);

    @Query("select p from PurchaseOrder p where p.invoice is not NULL")
    Collection<PurchaseOrder> findInvoiced();

    @Query("select p from PurchaseOrder p where p.invoice is not null and p.invoice.status = UNPAID and p.paymentSchedule <= :dueDate")
    Stream<PurchaseOrder> findOrdersNeedReminded(@Param("dueDate") LocalDate dueDate);
}
