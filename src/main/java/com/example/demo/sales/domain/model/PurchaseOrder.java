package com.example.demo.sales.domain.model;

import com.example.demo.common.domain.BusinessPeriod;
import com.example.demo.inventory.domain.model.PlantInventoryEntry;
import com.example.demo.inventory.domain.model.PlantReservation;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@NoArgsConstructor(force=true,access= AccessLevel.PROTECTED)
public class PurchaseOrder {
    @Id
    @GeneratedValue
    Long id;

    @OneToMany
    List<PlantReservation> reservations;

    @ManyToOne
    PlantInventoryEntry plant;

    @ManyToOne
    Customer customer;

    LocalDate issueDate;
    LocalDate paymentSchedule;

    @Column(precision=8,scale=2)
    BigDecimal total;

    @Enumerated(EnumType.STRING)
    POStatus status;

    @Embedded
    BusinessPeriod rentalPeriod;

    @Embedded
    PlantHireRequest plantHireRequest;

    @ElementCollection
    List<POExtension> extensions = new ArrayList<>();

    @OneToOne(cascade = CascadeType.ALL)
    Invoice invoice;

    public static PurchaseOrder of(PlantInventoryEntry entry, BusinessPeriod period, PlantHireRequest phr) {
        PurchaseOrder po = new PurchaseOrder();
        po.plant = entry;
        po.rentalPeriod = period;
        po.reservations = new ArrayList<>();
        po.issueDate = LocalDate.now();
        po.paymentSchedule = period.getEndDate().plusDays(3);
        po.status = POStatus.PENDING;
        po.plantHireRequest = phr;
        return po;
    }

    public void setStatus(POStatus newStatus) {
        status = newStatus;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public void setRentalPeriod(BusinessPeriod rentalPeriod) {
        this.rentalPeriod = rentalPeriod;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public void setPaymentSchedule(LocalDate paymentSchedule) {
        this.paymentSchedule = paymentSchedule;
    }
}
