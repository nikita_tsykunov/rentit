package com.example.demo.sales.domain.model;

public enum InvoiceStatus {
    UNPAID, APPROVED, REJECTED, LATE_PAYMENT, PAID
}
