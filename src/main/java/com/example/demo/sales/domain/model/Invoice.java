package com.example.demo.sales.domain.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
@Data
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
public class Invoice {
    @Id
    @GeneratedValue
    Long id;

    InvoiceStatus status;

    LocalDate invoiceDate;

    public static Invoice of()
    {
        Invoice invoice = new Invoice();
        invoice.invoiceDate = LocalDate.now();
        invoice.status = InvoiceStatus.UNPAID;

        return invoice;
    }
}
