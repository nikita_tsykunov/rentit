package com.example.demo.sales.application.dto;

import com.example.demo.sales.domain.model.InvoiceStatus;
import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

import java.time.LocalDate;

@Data
public class InvoiceDTO extends RepresentationModel<InvoiceDTO> {
    Long id;
    InvoiceStatus status;
    LocalDate invoiceDate;
}
