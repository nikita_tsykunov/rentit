package com.example.demo.sales.application.service;

import com.example.demo.common.application.service.BusinessPeriodValidator;
import com.example.demo.common.domain.BusinessPeriod;
import com.example.demo.inventory.domain.model.PlantInventoryEntry;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import com.example.demo.inventory.domain.model.PlantReservation;
import com.example.demo.inventory.domain.repository.InventoryRepository;
import com.example.demo.inventory.domain.repository.PlantInventoryEntryRepository;
import com.example.demo.inventory.domain.repository.PlantReservationRepository;
import com.example.demo.sales.application.dto.POExtensionDTO;
import com.example.demo.sales.application.dto.PlantHireRequestDTO;
import com.example.demo.sales.application.dto.PurchaseOrderDTO;
import com.example.demo.sales.domain.model.*;
import com.example.demo.sales.domain.repository.InvoiceRepository;
import com.example.demo.sales.domain.repository.PurchaseOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.DataBinder;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

@Service
public class SalesService {
    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepository;

    @Autowired
    PurchaseOrderRepository poRepository;

    @Autowired
    InventoryRepository inventoryRepository;

    @Autowired
    PlantReservationRepository plantReservationRepository;

    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    InvoiceRepository invoiceRepository;

    public PurchaseOrderDTO findPO(Long id) {
        PurchaseOrder po = poRepository.findById(id).orElse(null);
        return purchaseOrderAssembler.toModel(po);
    }

    public CollectionModel<PurchaseOrderDTO> findPurchaseOrders() {
        Collection<PurchaseOrder> entries = poRepository.findAll();
        return purchaseOrderAssembler.toCollectionModel(entries);
    }
    public CollectionModel<PurchaseOrderDTO> findInvoiced() {
        return purchaseOrderAssembler.toCollectionModel(poRepository.findInvoiced());
    }

    public CollectionModel<PurchaseOrderDTO> findPurchaseOrdersByCustomerName(String customerName) {
        Collection<PurchaseOrder> entries = poRepository.findByCustomerName(customerName);
        return purchaseOrderAssembler.toCollectionModel(entries);
    }

    public PurchaseOrderDTO createPO(PurchaseOrderDTO poDTO) {
        BusinessPeriod period = BusinessPeriod.of(
                         poDTO.getRentalPeriod().getStartDate(),
                         poDTO.getRentalPeriod().getEndDate());

        validatePeriod(period);

        if(poDTO.getPlant() == null)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid input Plant");

        PlantInventoryEntry plant = plantInventoryEntryRepository.findById(poDTO.getPlant().get_id()).orElse(null);

        if(plant == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Plant not Found");

        PlantHireRequest phr = PlantHireRequest.of(poDTO.getPlantHireRequest().getLink("self").get().getHref());


        PurchaseOrder po = PurchaseOrder.of(plant, period, phr);
        po.setTotal(plant.getPrice().multiply(BigDecimal.valueOf(ChronoUnit.DAYS.between(po.getRentalPeriod().getStartDate(),
                po.getRentalPeriod().getEndDate()))));
        poRepository.save(po);

        List<PlantInventoryItem> availableItems = inventoryRepository.findAvailableItems(
                po.getPlant().getId(),
                po.getRentalPeriod().getStartDate(),
                po.getRentalPeriod().getEndDate());

        if(availableItems.isEmpty()) {
            po.setStatus(POStatus.REJECTED);
            poRepository.save(po);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No available items");
        }

        poRepository.save(po);
        return purchaseOrderAssembler.toModel(po);

    }

    public PurchaseOrderDTO modifyPO(Long id, PurchaseOrderDTO poDTO) {
        PurchaseOrder oldPO = poRepository.findById(id).orElse(null);

        if (oldPO == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No PO with such id");
        if (oldPO.getStatus() != POStatus.PENDING && oldPO.getStatus() != POStatus.OPEN)
            throw new ResponseStatusException(HttpStatus.CONFLICT, "PO cannot be modified because it is not pending or open");

        BusinessPeriod period = BusinessPeriod.of(
                poDTO.getRentalPeriod().getStartDate(),
                poDTO.getRentalPeriod().getEndDate());


        if (period != oldPO.getRentalPeriod()) {
            validatePeriod(period);
            oldPO.setRentalPeriod(period);
            oldPO.setPaymentSchedule(period.getEndDate().plusDays(3));
        }

        if (oldPO.getStatus() != POStatus.PENDING) {
            PlantReservation reservation = new PlantReservation();
            reservation.setSchedule(period);

            if (poDTO.getPlant() == null)
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid input Plant");

            List<PlantInventoryItem> availableItems = inventoryRepository.findAvailableItems(
                    poDTO.getPlant().get_id(),
                    poDTO.getRentalPeriod().getStartDate(),
                    poDTO.getRentalPeriod().getEndDate());

            if (availableItems.isEmpty()) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No available items");
            }

            // Remove old reservations
            while (!oldPO.getReservations().isEmpty())
                plantReservationRepository.delete(oldPO.getReservations().remove(0));

            // And add new
            reservation.setPlant(availableItems.get(0));
            plantReservationRepository.save(reservation);
            oldPO.getReservations().add(reservation);

            //oldPO.setStatus(POStatus.PENDING);
        }
        poRepository.save(oldPO);

        return purchaseOrderAssembler.toModel(oldPO);
    }

    private void validatePeriod(BusinessPeriod period) {
        DataBinder binder = new DataBinder(period);
        binder.addValidators(new BusinessPeriodValidator());
        binder.validate();

        if (binder.getBindingResult().hasErrors())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid PO Period");
    }

    public PurchaseOrderDTO acceptPO(Long id) {
        PurchaseOrder po = getPO(id);
        try {
            PlantHireRequestDTO phrDTO = restTemplate.getForObject(po.getPlantHireRequest().getHref(), PlantHireRequestDTO.class);
            String acceptLink = phrDTO.getXlink("notify accept");
            restTemplate.postForLocation(acceptLink, null);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Unable to notify client about purchase order acceptance");
        }

        List<PlantInventoryItem> availableItems = inventoryRepository.findAvailableItems(
                po.getPlant().getId(),
                po.getRentalPeriod().getStartDate(),
                po.getRentalPeriod().getEndDate());

        if(availableItems.size() == 0) {
            po.setStatus(POStatus.REJECTED);
            poRepository.save(po);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No available items");
        }

        PlantReservation reservation = new PlantReservation();
        reservation.setSchedule(po.getRentalPeriod());
        reservation.setPlant(availableItems.get(0));

        plantReservationRepository.save(reservation);
        po.getReservations().add(reservation);

        po.setStatus(POStatus.OPEN);
        poRepository.save(po);

        return purchaseOrderAssembler.toModel(po);
    }

    public PurchaseOrderDTO dispatchPO(Long id) {
        PurchaseOrder po = poRepository.findById(id).orElse(null);

        po.setStatus(POStatus.DISPATCHED);
        poRepository.save(po);

        return purchaseOrderAssembler.toModel(po);
    }

    public PurchaseOrderDTO rejectPO(Long id) {
        PurchaseOrder po = getPO(id);

        try {
            PlantHireRequestDTO phrDTO = restTemplate.getForObject(po.getPlantHireRequest().getHref(), PlantHireRequestDTO.class);
            String rejectLink = phrDTO.getXlink("notify reject");
            restTemplate.postForLocation(rejectLink, null);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Unable to notify client about purchase order rejection");
        }

        while (!po.getReservations().isEmpty()) {
            plantReservationRepository.delete(po.getReservations().remove(0));
        }
        po.setStatus(POStatus.REJECTED);
        poRepository.save(po);
        return purchaseOrderAssembler.toModel(po);
    }

    public PurchaseOrderDTO cancelPO(Long id) {
        PurchaseOrder po = poRepository.findById(id).orElse(null);

        if (po.getStatus() != POStatus.DISPATCHED && LocalDate.now().plusDays(1).isBefore(po.getRentalPeriod().getStartDate())){
            while (!po.getReservations().isEmpty()) {
                plantReservationRepository.delete(po.getReservations().remove(0));
            }
            po.setStatus(POStatus.CLOSED);
        }
        poRepository.save(po);

        return purchaseOrderAssembler.toModel(po);
    }
    public PurchaseOrderDTO extendPO(POExtensionDTO extension, Long id) {
        PurchaseOrder po = poRepository.findById(id).orElse(null);
        if (po == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "PO Not Found");
        if (po.getStatus() != POStatus.PENDING && po.getStatus() != POStatus.OPEN)
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Unable to extend PO that is not pending or open");
        BusinessPeriod period = BusinessPeriod.of(
                po.getRentalPeriod().getStartDate(),
                extension.getEndDate());
        po.setRentalPeriod(period);
        po.setPaymentSchedule(extension.getEndDate().plusDays(3));

        while (!po.getReservations().isEmpty()) {
            plantReservationRepository.delete(po.getReservations().remove(0));
        }
        po.setStatus(POStatus.PENDING);

        poRepository.save(po);
        return purchaseOrderAssembler.toModel(po);
    }

    public PurchaseOrderDTO returnPlant(Long id) {
        PurchaseOrder po = poRepository.findById(id).orElse(null);

        if(po == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "PO Not Found");
        }

        Invoice invoice = Invoice.of();

        po.setInvoice(invoice);
        poRepository.save(po);

        try {
            PlantHireRequestDTO phrDTO = restTemplate.getForObject(po.getPlantHireRequest().getHref(), PlantHireRequestDTO.class);
            if (phrDTO != null) {
                String submitInvoice = phrDTO.getXlink("submit invoice");
                restTemplate.postForLocation(submitInvoice, null);
            } else {
                throw new Exception();
            }
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Unable to submit invoice for client");
        }

        while (!po.getReservations().isEmpty()) {
            plantReservationRepository.delete(po.getReservations().remove(0));
        }

        po.setStatus(POStatus.RETURNED);
        poRepository.save(po);

        return purchaseOrderAssembler.toModel(po);
    }

    @Scheduled(cron = "0 0 10 ? * MON-FRI")
    @Transactional(propagation= Propagation.REQUIRED, noRollbackFor=Exception.class)
    public void sendReminders() {
        try (Stream<PurchaseOrder> orderStream = poRepository.findOrdersNeedReminded(LocalDate.now().plusDays(1))) {
            orderStream.forEach((order) -> sendReminder(order.getId()));
        }
    }

    @Transactional(propagation= Propagation.REQUIRED, noRollbackFor=Exception.class)
    public PurchaseOrderDTO sendReminder(Long id) {
        PurchaseOrder po = poRepository.findById(id).orElse(null);

        if (po == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No PHR with such id");
        }

        if (po.getInvoice() == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "There is no invoice for this Purchase Order");
        } else if (po.getInvoice().getStatus() != InvoiceStatus.UNPAID){
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Unable to send reminder to client");
        } else {
            po.getInvoice().setStatus(InvoiceStatus.LATE_PAYMENT);
            try {
                PlantHireRequestDTO phrDTO = restTemplate.getForObject(po.getPlantHireRequest().getHref(), PlantHireRequestDTO.class);
                if (phrDTO != null) {
                    String sendReminder = phrDTO.getXlink("remind");
                    restTemplate.postForLocation(sendReminder, null);
                } else {
                    throw new Exception();
                }
            } catch (Exception ex) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Unable to send reminder to client");
            }
        }

        poRepository.save(po);

        return purchaseOrderAssembler.toModel(po);
    }

    @Transactional(propagation= Propagation.REQUIRED, noRollbackFor=Exception.class)
    public PurchaseOrderDTO getRemittanceAdvice(Long id) {
        PurchaseOrder po = poRepository.findById(id).orElse(null);

        if (po == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No PHR with such id");
        }

        if (po.getInvoice() == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "There is no invoice for this PHR");
        } else if (po.getInvoice().getStatus() == InvoiceStatus.REJECTED){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Invoice can't be approved");
        } else {
            po.getInvoice().setStatus(InvoiceStatus.PAID);
        }

        poRepository.save(po);

        return purchaseOrderAssembler.toModel(po);
    }

    private PurchaseOrder getPO(Long id) {
        PurchaseOrder po = poRepository.findById(id).orElse(null);
        if(po == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "PO Not Found");
        if(po.getStatus() != POStatus.PENDING)
            throw new ResponseStatusException(HttpStatus.CONFLICT, "PO cannot be accepted/rejected because it is not Pending");
        return po;
    }

    // Only for Cucumber
    public void deleteAllPOs() {
        poRepository.deleteAll();
        plantReservationRepository.deleteAll();
    }

    // Only for Cucumber
    public void remindAllPOs() {
        for(PurchaseOrder po: poRepository.findByStatus(POStatus.RETURNED)) {
            sendReminder(po.getId());
        }
    }
}
