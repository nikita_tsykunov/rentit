package com.example.demo.sales.application.service;

import com.example.demo.sales.application.dto.InvoiceDTO;
import com.example.demo.sales.domain.model.Invoice;
import com.example.demo.sales.rest.SalesRestController;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Service;

@Service
public class InvoiceAssembler extends RepresentationModelAssemblerSupport<Invoice, InvoiceDTO> {

    public InvoiceAssembler() { super(SalesRestController.class, InvoiceDTO.class); }

    @Override
    public InvoiceDTO toModel(Invoice invoice) {
        InvoiceDTO dto = new InvoiceDTO();

        dto.setId(invoice.getId());
        dto.setInvoiceDate(invoice.getInvoiceDate());
        dto.setStatus(invoice.getStatus());

        return dto;
    }
}
