package com.example.demo.sales.application.dto;

import com.example.demo.common.application.dto.BusinessPeriodDTO;
import com.example.demo.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.demo.sales.domain.model.POStatus;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.RepresentationModel;

import java.math.BigDecimal;
import java.time.LocalDate;


@Data
public class PurchaseOrderDTO extends RepresentationModel<PurchaseOrderDTO> {
    Long _id;
    BusinessPeriodDTO rentalPeriod;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    LocalDate paymentSchedule;
    PlantInventoryEntryDTO plant;
    POStatus status;
    PlantHireRequestDTO plantHireRequest;
    InvoiceDTO invoice;
    BigDecimal total;
}
