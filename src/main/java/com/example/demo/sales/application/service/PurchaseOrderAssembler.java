package com.example.demo.sales.application.service;

import com.example.demo.common.application.dto.BusinessPeriodDTO;
import com.example.demo.inventory.application.service.PlantInventoryEntryAssembler;
import com.example.demo.sales.application.dto.PlantHireRequestDTO;
import com.example.demo.sales.application.dto.PurchaseOrderDTO;
import com.example.demo.sales.domain.model.InvoiceStatus;
import com.example.demo.sales.domain.model.PurchaseOrder;
import com.example.demo.sales.rest.SalesRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Service
public class PurchaseOrderAssembler extends RepresentationModelAssemblerSupport<PurchaseOrder, PurchaseOrderDTO> {

    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;

    @Autowired
    InvoiceAssembler invoiceAssembler;

    public PurchaseOrderAssembler() {
        super(SalesRestController.class, PurchaseOrderDTO.class);
    }

    @Override
    public PurchaseOrderDTO toModel(PurchaseOrder po) {
        PurchaseOrderDTO dto = new PurchaseOrderDTO();
        dto.setStatus(po.getStatus());
        dto.set_id(po.getId());
        dto.setRentalPeriod(BusinessPeriodDTO.of(po.getRentalPeriod().getStartDate(), po.getRentalPeriod().getEndDate()));
        dto.setPaymentSchedule(po.getPaymentSchedule());
        dto.setPlant(plantInventoryEntryAssembler.toModel(po.getPlant()));
        dto.setPlantHireRequest(PlantHireRequestDTO.of(po.getPlantHireRequest().getHref()));
        dto.setTotal(po.getTotal());

        if (po.getInvoice() != null) {
            dto.setInvoice(invoiceAssembler.toModel(po.getInvoice()));
        }

        dto.add(linkTo(methodOn(SalesRestController.class)
                .fetchPurchaseOrder(dto.get_id())).withRel("self")
                .withType(HttpMethod.GET.toString())
                .withName("self"));
        try {
            switch (po.getStatus()) {
                case PENDING:
                    dto.add(linkTo(methodOn(SalesRestController.class)
                            .acceptPurchaseOrder(dto.get_id())).withRel("accept")
                            .withType(HttpMethod.POST.toString())
                            .withName("accept"));
                    dto.add(linkTo(methodOn(SalesRestController.class)
                            .rejectPurchaseOrder(dto.get_id())).withRel("reject")
                            .withType(HttpMethod.DELETE.toString())
                            .withName("reject"));
                    dto.add(linkTo(methodOn(SalesRestController.class)
                            .cancelPurchaseOrder(dto.get_id())).withRel("cancel")
                            .withType(HttpMethod.POST.toString()));
                    break;
                case OPEN:
                    dto.add(linkTo(methodOn(SalesRestController.class)
                            .requestPurchaseOrderExtension(null, dto.get_id()))
                            .withRel("extend")
                            .withType(HttpMethod.POST.toString())
                            .withName("extend"));
                    dto.add(linkTo(methodOn(SalesRestController.class)
                            .cancelPurchaseOrder(dto.get_id()))
                            .withRel("cancel")
                            .withType(HttpMethod.POST.toString()));
                    dto.add(linkTo(methodOn(SalesRestController.class)
                            .dispatchPurchaseOrder(dto.get_id()))
                            .withRel("dispatch")
                            .withType(HttpMethod.POST.toString())
                            .withName("dispatch"));
                    break;
                case DISPATCHED:
                    dto.add(linkTo(methodOn(SalesRestController.class)
                            .requestPurchaseOrderExtension(null, dto.get_id()))
                            .withRel("extend")
                            .withType(HttpMethod.POST.toString())
                            .withName("extend"));
                    dto.add(linkTo(methodOn(SalesRestController.class)
                            .returnPlant(dto.get_id())).withRel("returned")
                            .withType(HttpMethod.POST.toString())
                            .withName("returned"));
                    dto.add(linkTo(methodOn(SalesRestController.class)
                            .cancelPurchaseOrder(dto.get_id())).withRel("cancel")
                            .withType(HttpMethod.POST.toString()));
                    break;
                case REJECTED:
                    dto.add(linkTo(methodOn(SalesRestController.class)
                            .modifyPO(dto.get_id(), null)).withRel("modify")
                            .withType(HttpMethod.PATCH.toString()));
                case RETURNED:
                    if (po.getInvoice() != null && po.getInvoice().getStatus() != InvoiceStatus.PAID) {
                       dto.add(linkTo(methodOn(SalesRestController.class)
                                .remittanceAdvice(dto.get_id())).withRel("remittance")
                                .withType(HttpMethod.POST.toString())
                                .withName("remittance"));
                    }
                    break;
                default:
                    break;
            }
        } catch (Exception e) {}

        return dto;
    }
}
