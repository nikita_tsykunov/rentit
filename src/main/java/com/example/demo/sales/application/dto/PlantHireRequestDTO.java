package com.example.demo.sales.application.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor(force = true)
public class PlantHireRequestDTO extends RepresentationModel<PlantHireRequestDTO> {
    String href;
    List<Map<String, String>> _xlinks;

    public String getXlink(String ref) {
        for(Map<String, String> link : _xlinks) {
            if(ref.equals(link.get("_rel")))
                return link.get("href");
        }
        return null;
    }

    public static PlantHireRequestDTO of(String href) {
        PlantHireRequestDTO phrDTO = new PlantHireRequestDTO();
        phrDTO.href = href;
        phrDTO._xlinks = new ArrayList<>();
        return phrDTO;
    }
}
