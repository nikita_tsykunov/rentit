package com.example.demo.sales.rest;

import com.example.demo.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.demo.inventory.application.service.InventoryService;
import com.example.demo.sales.application.dto.POExtensionDTO;
import com.example.demo.sales.application.dto.PurchaseOrderDTO;
import com.example.demo.sales.application.service.SalesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

@RestController
@RequestMapping("/api/sales")
@CrossOrigin
public class SalesRestController {
    @Autowired
    InventoryService inventoryService;

    @Autowired
    SalesService salesService;

    @GetMapping("/plants")
    public CollectionModel<PlantInventoryEntryDTO> findAvailablePlants(
            @RequestParam(name = "name") String plantName,
            @RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate
    ) {
        return inventoryService.findAvailablePlants(plantName.toLowerCase(), startDate, endDate);
    }

    @GetMapping("/orders/{id}")
    @ResponseStatus(HttpStatus.OK)
    public PurchaseOrderDTO fetchPurchaseOrder(@PathVariable("id") Long id) {
        return salesService.findPO(id);
    }

    @PatchMapping("/orders/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<PurchaseOrderDTO> modifyPO(@PathVariable("id") Long id, @RequestBody PurchaseOrderDTO po)
    {
        return new ResponseEntity<>(salesService.modifyPO(id, po), HttpStatus.OK);
    }

    @GetMapping("/orders")
    public CollectionModel<PurchaseOrderDTO> findOrders(
            @RequestParam(name = "customerName", required = false) String customerName) {
        if (customerName != null) {
            return salesService.findPurchaseOrdersByCustomerName(customerName);
        }
        return salesService.findPurchaseOrders();
    }

    @GetMapping("/orders/invoice")
    public CollectionModel<PurchaseOrderDTO> findInvoiced()
    {
        return salesService.findInvoiced();
    }

    @PostMapping("/orders")
    public ResponseEntity<?> createPurchaseOrder(@RequestBody PurchaseOrderDTO partialPODTO) {
        PurchaseOrderDTO newlyCreatePODTO = salesService.createPO(partialPODTO);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(newlyCreatePODTO.getRequiredLink(IanaLinkRelations.SELF).toUri());

        return new ResponseEntity<>(newlyCreatePODTO, headers, HttpStatus.CREATED);
    }

    @PostMapping("/orders/{id}/cancel")
    public PurchaseOrderDTO cancelPurchaseOrder(@PathVariable Long id) {
        return salesService.cancelPO(id);
    }

    @PostMapping("/orders/{id}/accept")
    public PurchaseOrderDTO acceptPurchaseOrder(@PathVariable Long id) {
        return salesService.acceptPO(id);
    }

    @PostMapping("/orders/{id}/dispatch")
    public PurchaseOrderDTO dispatchPurchaseOrder(@PathVariable Long id) {
        return salesService.dispatchPO(id);
    }

    @DeleteMapping("/orders/{id}/reject")
    public PurchaseOrderDTO rejectPurchaseOrder(@PathVariable Long id) {
        return salesService.rejectPO(id);
    }

    @PostMapping("orders/{id}/returnPlant")
    public PurchaseOrderDTO returnPlant(@PathVariable Long id) {
        return salesService.returnPlant(id);
    }

    @PostMapping("orders/{id}/invoice/remittance")
    public PurchaseOrderDTO remittanceAdvice(@PathVariable Long id) {
        return salesService.getRemittanceAdvice(id);
    }

    @PostMapping("orders/{id}/invoice/reminder")
    public PurchaseOrderDTO sendReminder(@PathVariable Long id) {
        return salesService.sendReminder(id);
    }

    @PostMapping("/orders/{id}/extensions")
    public PurchaseOrderDTO requestPurchaseOrderExtension(@RequestBody POExtensionDTO extension, @PathVariable("id") Long id) {
        return salesService.extendPO(extension, id);
    }

    // Only for Cucumber
    @DeleteMapping("/orders")
    public void deleteAllPOs() {
        salesService.deleteAllPOs();
    }

    // Only for Cucumber
    @PostMapping("/orders/remind")
    public void remindAllPOs() {
        salesService.remindAllPOs();
    }
}