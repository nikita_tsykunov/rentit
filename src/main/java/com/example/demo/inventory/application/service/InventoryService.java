package com.example.demo.inventory.application.service;

import com.example.demo.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.demo.inventory.domain.model.PlantInventoryEntry;
import com.example.demo.inventory.domain.repository.InventoryRepository;
import com.example.demo.inventory.domain.repository.PlantReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.Collection;

@Service
public class InventoryService {

    @Autowired
    InventoryRepository inventoryRepository;

    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;

    @Autowired
    PlantReservationRepository plantReservationRepository;

    public CollectionModel<PlantInventoryEntryDTO> findAvailablePlants(String name, LocalDate startDate, LocalDate endDate) {
        Collection<PlantInventoryEntry> entries = inventoryRepository.findAvailablePlants(name, startDate, endDate);
        return plantInventoryEntryAssembler.toCollectionModel(entries);
    }

    public CollectionModel<PlantInventoryEntryDTO> getAllPlants() {
        Collection<PlantInventoryEntry> entries = inventoryRepository.findAll();
        return plantInventoryEntryAssembler.toCollectionModel(entries);
    }

    public PlantInventoryEntryDTO fetchPlant(Long id) {
        PlantInventoryEntry plant = inventoryRepository.findById(id).orElse(null);
        if(plant == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No plant with such id");
        return plantInventoryEntryAssembler.toModel(plant);
    }
}