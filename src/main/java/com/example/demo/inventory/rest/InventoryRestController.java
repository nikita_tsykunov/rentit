package com.example.demo.inventory.rest;

import com.example.demo.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.demo.inventory.application.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/inventory")
@CrossOrigin
public class InventoryRestController {
    @Autowired
    InventoryService inventoryService;

    @GetMapping("/plants/{id}")
    @ResponseStatus(HttpStatus.OK)
    public PlantInventoryEntryDTO fetchPlant(@PathVariable("id") Long id) {
        return inventoryService.fetchPlant(id);
    }

    @GetMapping("/plants")
    public CollectionModel<PlantInventoryEntryDTO> getAllPlants() {
        return inventoryService.getAllPlants();
    }
}
